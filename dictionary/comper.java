package dictionary;

import java.util.Comparator;
import java.util.Map;

public class comper {
	public static class ValueComparator implements Comparator<Map.Entry<String, Integer>>{  
	    public int compare(Map.Entry<String, Integer> mp1, Map.Entry<String, Integer> mp2){  
	        return mp2.getValue() - mp1.getValue();  
	    }
	}
	public static class keycomparator implements Comparator<Map.Entry<String, Integer>>{
		public int compare(Map.Entry<String, Integer> o1,  
                Map.Entry<String, Integer> o2) {  
            return (o1.getKey()).toString().compareTo(o2.getKey().toString());  
        }  
	}
}
