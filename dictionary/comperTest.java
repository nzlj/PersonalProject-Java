package dictionary;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Test;

public class comperTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Map<String, Integer> map = new ConcurrentHashMap<String, Integer>();
		map.put("temp", 2);
		map.put("apple", 2);
		map.put("bettle", 1);
		map.put("counting", 1);
		List<Map.Entry<String, Integer>> infoIds = new ArrayList<Map.Entry<String, Integer>>(map.entrySet());
		comper.keycomparator kc=new dictionary.comper.keycomparator();
		Collections.sort(infoIds, kc);
		
		comper.ValueComparator vc=new dictionary.comper.ValueComparator();
		Collections.sort(infoIds,vc);
		Iterator<Map.Entry<String,Integer>> it=infoIds.iterator();
		
			Entry entry= it.next();
			assertEquals("apple",entry.getKey());
			assertEquals(2,entry.getValue());
			entry= it.next();
			assertEquals("temp",entry.getKey());
			assertEquals(2,entry.getValue());
			entry= it.next();
			assertEquals("bettle",entry.getKey());
			assertEquals(1,entry.getValue());
			entry= it.next();
			assertEquals("counting",entry.getKey());
			assertEquals(1,entry.getValue());
		
	}

}
