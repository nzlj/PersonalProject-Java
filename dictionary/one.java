package dictionary;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.io.FileInputStream;

/**
 * 使用FileInputStream读取文件
 */
public class one {
	private static class ValueComparator implements Comparator<Map.Entry<String, Integer>>{  
        public int compare(Map.Entry<String, Integer> mp1, Map.Entry<String, Integer> mp2){  
            return mp2.getValue() - mp1.getValue();  
        }  
    }  

         public static void main(String[] args) {
                   //声明流对象
                   FileInputStream fis = null;

                   try{
                            //创建流对象
                            fis = new FileInputStream("D:/"+args[0]);
                            //读取数据，并将读取到的数据存储到数组中
                            byte[] data = new byte[1024]; //数据存储的数组
                            int kong=0;
                            int t=0;
                            int n=0;
                            byte[] rold = new byte[10];
                            int flag=0;
                            Map<String, Integer> map = new ConcurrentHashMap<String, Integer>();
                            //List<Map.Entry<String,Integer>> list=new ArrayList<>();

                            int i = fis.read(data);

                            //解析数据
                            String s = new String(data,0,i);
                            //输出字符串
                            System.out.println(s.toLowerCase());
                            for(int x=0;x<=i;x++) {
                            	switch(data[x]) {
                            	case 32:
                            		kong++;break;
                            	case 9:
                            		t++;break;
                            	case 10:
                            		n++;break;
                            	}
                            	
                            	if(data[x]>=65&&data[x]<=90) {
                            		rold[flag]=data[x];
                            		flag++;
                            	}
                            	else if(data[x]>=97&&data[x]<=122) {
                            		rold[flag]=data[x];
                            		flag++;
                            	}
                            	
                            	else if(data[x]==32||data[x]==10||data[x]==13||data[x]==0){
                            		if(flag>3) {
                            				String str = new String(rold).trim();
                            				Iterator<Entry<String, Integer>> it = map.entrySet().iterator();
                            				if(it.hasNext()) {
                            		        while(it.hasNext()) {
                            		        	Entry<String, Integer> entry = it.next();
                            		        	
                            		            if(entry.getKey().equals(str)) {
                            		            	
                            		            	map.put(str, entry.getValue()+1);
                            		            	flag=0;
                                					rold = new byte[10];
                                					break;
                            		            }
                            		            else {
                            		            	
                            		            	map.put(str, 1);
                            		            	flag=0;
                                					rold = new byte[10];
                            		            }
                            		            
                            		        }
                            		        
                            				}
                            				else {
                            					map.put(str, 1);
                            					flag=0;
                            					rold = new byte[10];
                            					
                            				}
                            		}
                            		else {
                            		flag=0;
                            		rold = new byte[10];
                            		}
                            	}
                            	else if(data[x]>=48&&data[x]<=57) {
                            		
                            		if(data[x]!=32&&flag>3) {
                            			rold[flag]=data[x];
                        				flag++;
                            		}
                            		else {
                            			flag=0;
                                		rold = new byte[10];
                            		}
                    			}
                            }
                            System.out.println("空格数为："+kong);
                            System.out.println("水平制表符数为："+t);
                            System.out.println("换行符数为："+n);

                            
                            List<Map.Entry<String,Integer>> list=new ArrayList<>();
                    		list.addAll(map.entrySet());
                    		one.ValueComparator vc=new ValueComparator();
                    		Collections.sort(list,vc);
                    		Object[] key = map.keySet().toArray();
                    		Arrays.sort(key);
                    		for(Iterator<Map.Entry<String,Integer>> it=list.iterator();it.hasNext();){
                    			System.out.println(it.next());
                    		}
                   }catch(Exception e){
                            e.printStackTrace();
                   }finally{
                            try{
                                     //关闭流，释放资源
                                     fis.close();
                            }catch(Exception e){}
                   }
         }
}

