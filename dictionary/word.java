package dictionary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class word {
	public Iterator<Entry<String, Integer>> word(byte[] data,int i){
		byte[] rold = new byte[20];
        int flag=0;
        Map<String, Integer> map = new ConcurrentHashMap<String, Integer>();
        int rf=0;
		for(int x=0;x<=i;x++) {				//遍历byte数组
			if(data[x]>=65&&data[x]<=90) {	//如果为大写改为小写
				data[x]=(byte) (data[x]+32);
        		rold[flag]=data[x];			//是字母计入rold数组
        		flag++;						//统计单词长度
        	}
        	else if(data[x]>=97&&data[x]<=122) {//是小写字母计入数组
        		rold[flag]=data[x];
        		flag++;							//放入数组单词长度加一
        	}
        	
        	else if(data[x]==32||data[x]==10||data[x]==13||data[x]==0||data[x]==9){//如是空格回车TAB分隔符一类的进入判断
        		if(flag>3) {							//字母数长度超过3可以算作一个单词
        				String str = new String(rold).trim();	//使rold数组转为string并取掉多余的空格
        				Iterator<Entry<String, Integer>> it = map.entrySet().iterator();//用迭代器遍历判断是否有相同的单词已经存入
        				if(it.hasNext()) {
        		        while(it.hasNext()) {
        		        	Entry<String, Integer> entry = it.next();
        		            if(entry.getKey().equals(str)) {//如有相同的单词，value+1
        		            	map.put(str, entry.getValue()+1);
        		            	flag=0;						//单词长度清空
            					rold = new byte[20];		//数组清空
            					rf=1;						//记录是否放入了map
            					break;
        		            }
        		        }
        		        if(rf!=1) {     		//循环完了，没放入需要放入
        		        	map.put(str, 1);
        		        	flag=0;
        					rold = new byte[20];
        		        }
        		        rf=0;					//置0再次使用
        				}
        				else {					//如果map里没值直接put
        					map.put(str, 1);
        					flag=0;
        					rold = new byte[20];
        				}
        		}
        		else {					//如果不满足4个字母数，清空
        		flag=0;
        		rold = new byte[20];
        		}
        	}
        	else if(data[x]>=48&&data[x]<=57) {		//如碰到特殊字符判断下一个字符是不是空并且字母数超过3个可以计入单词
        		if(data[x]!=32&&flag>3) {
        			rold[flag]=data[x];
    				flag++;
        		}
        		else {						//如是空格或者不到4个字母清空
        			flag=0;
            		rold = new byte[20];
            		while(data[x+1]!=32) {	//以特殊字符和数字开头的都不算单词
            			x++;
            			if(x==i)
            				break;
            		}
        		}
			}
		}				//遍历完之后最后一个单词可能大于3个字母数需要计入数组运行如上
		 if(flag>3) {
			 String str = new String(rold).trim();
				Iterator<Entry<String, Integer>> it = map.entrySet().iterator();
				if(it.hasNext()) {
		        while(it.hasNext()) {
		        	Entry<String, Integer> entry = it.next();
		        	
		            if(entry.getKey().equals(str)) {
		            	
		            	map.put(str, entry.getValue()+1);
		            	flag=0;
 					rold = new byte[20];
 					break;
		            }
		            else {
		            	
		            	map.put(str, 1);
		            	flag=0;
 					rold = new byte[20];
 					break;
		            }
		            
		        }
		        
				}
				else {
					map.put(str, 1);
					flag=0;
					rold = new byte[20];
					
				}
		 }
        
		
		List<Map.Entry<String, Integer>> infoIds = new ArrayList<Map.Entry<String, Integer>>(map.entrySet());
		comper.keycomparator kc=new dictionary.comper.keycomparator();
		Collections.sort(infoIds, kc);
		
		comper.ValueComparator vc=new dictionary.comper.ValueComparator();
		Collections.sort(infoIds,vc);
		Iterator<Map.Entry<String,Integer>> it=infoIds.iterator();

		for(int num=0;num<10;num++){
			if(it.hasNext()) {
			Entry entry= it.next();
			System.out.println(entry.getKey()+":"+entry.getValue());      // 获取key
			}
		}
		return it;
	}
}
