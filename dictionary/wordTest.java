package dictionary;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

public class wordTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testWord() {
		String str = "hello hello2	Hello /r/n hEllo java201621123036 1file apple";
		byte[] data = str.getBytes();
		word w=new word();
		Iterator<Map.Entry<String,Integer>> it=w.word(data, str.length()-1);
		
			
			Entry entry= it.next();
			assertEquals("hello",entry.getKey());
			assertEquals("3",entry.getValue());
			entry= it.next();
			assertEquals("apple",entry.getKey());
			assertEquals("1",entry.getValue());
			entry= it.next();
			assertEquals("hello2",entry.getKey());
			assertEquals("1",entry.getValue());
			entry= it.next();
			assertEquals("hello201621123036",entry.getKey());
			assertEquals("1",entry.getValue());
			
			
			
		
	}
}
