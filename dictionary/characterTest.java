package dictionary;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class characterTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCharacter() {
		
		String str = "Hello201621123036 hello 	\r\n";
		byte[] data = str.getBytes();
		character c=new character(data,26);
		assertEquals(2,c.kong);
		assertEquals(1,c.t);
		assertEquals(1,c.n);
	}
}
