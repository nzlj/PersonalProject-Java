package dictionary;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class wordsimpleTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testWordsimple() {
		String str = "Hello201621123036 hello	Hello \r\n heLLo be cass+ cas- 1file 2file";
		byte[] data = str.getBytes();
		wordsimple w=new wordsimple(data,str.length()-1);
		assertEquals(5,w.num);
	}

}
